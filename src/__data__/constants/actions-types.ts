// Запрос авторизации.
export const AUTH_FETCH = 'AUTH_FETCH';
// Успешно завершенный запрос авторизации.
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
// Не успешно завершенный запрос авторизации.
export const AUTH_FAILURE = 'AUTH_FAILURE';
