import { } from '../constants/actions-types';

export interface LoadingState {
    loading: Boolean;
}

const initialState = {
    loading: false,
};

export const login = (state: LoadingState = initialState, action: any) => state;
