import * as React from "react";
import { connect } from 'react-redux';

import { login } from './__data__/reducers';

import { AdminPage } from './pages/login-page'

class App extends React.PureComponent<any, any> {
    render() {
        // console.log('app.props', this.props);
        return (
            <AdminPage {...this.props}/>
       )
    }
}

const connectedApp = connect(null)(App)
connectedApp['reducer'] = login;
export default connectedApp;