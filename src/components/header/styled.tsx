import styled from 'styled-components';
import { Logo, MenuIcon } from 'assets';
import { device } from '__data__/constants';

export const Wrapper = styled.section`
  height: 72px;
  font-family: Montserrat;
  font-size: 12px;
  font-weight: 600;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.17;
  letter-spacing: normal;
  color: #1e1d20;
  display: flex;
  padding: 0;

  @media ${device.tablet} {
    padding: 0 20px;
  }
`;

export const StyledLogo = styled(Logo)`
  cursor: pointer;
  @media ${device.tablet} {
    width: 267px;
    height: 48px;
    margin-left: 0;
  }
`;

export const LogoBlock = styled.a`
  padding-left: 24px;
  display: flex;
  justify-content: center;
  flex-direction: column;
  text-decoration: none;
  font-size: 22px;
  font-weight: 600;
  color: #1e1d20;

  @media ${device.tablet} {
    padding: 0;
    
  }
`;

export const StyledMenu = styled(MenuIcon)`
  position: absolute;
  right: 24px;
  top: 24px;
  cursor: pointer;
  @media ${device.tablet} {
    display: none;
  }
`;

export const NavBlock = styled.div`
  display: none;
  align-self: center;
  width: 100%;
  justify-content: flex-end;

  @media ${device.tablet} {
    display: flex;
  }
`;

export const NavLink: any = styled.span`
  display: block;
  height: 20px;
  padding-top: 12px;
  margin-bottom: 10px;
  color: #003eff;
  text-transform: uppercase;
  align-self: flex-end;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  text-decoration: none;
  font-size: 12px;
  margin-right: 10px;
  cursor: pointer;

  @media ${device.tablet} {
    display: inline-block;
    font-size: 14px;
    margin-right: 20px;
    ${(props: any) => props.selected ? 'border-bottom: 3px solid #003eff;' : ''}
    ${(props: any) => props.selected ? 'margin-bottom: 7px;' : ''}

    &:hover, &:visited {
      text-decoration: none;
      ${(props: any) => props.selected ? 'border-bottom: 3px solid #003eff;' : ''}
      ${(props: any) => props.selected ? 'margin-bottom: 7px;' : ''}
      color: #003eff;
    }
  }
`;

export const NavLinkStub = styled.div`
  font-size: 14px;
  color: #c2c2c2;
  text-transform: uppercase;
  align-self: center;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  text-decoration: none;

`;

export const PhoneBlock = styled.div`
    display: grid;
    grid-template-columns: 24px auto;
    grid-column-gap: 8px;
    margin-top: 58px;
    > div {
    align-self: center;
    }
  @media ${device.tablet} {
    margin-top: 0px;
  }
`;

export const PhoneText = styled.a`
    display: flex;
    align-items: center;
    text-decoration: none;
    font-size: 16px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: right;
    color: #1e1d20;
`;

export const MenuBlock = styled.div`
    position: absolute;
    top: 72px;
    z-index: 1000;
    width: 100%;
    background-color: white;
    user-select: none;
    height: 100vh;
    padding-left: 24px;
`;