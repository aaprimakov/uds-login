import * as React from 'react';

import { LogoBlock, NavBlock, NavLink, PhoneBlock, PhoneText, Wrapper, NavLinkStub, MenuBlock, StyledLogo, StyledMenu } from './styled';
import { phone } from 'assets';

export class Header extends React.PureComponent <any, any> {
  state = { menuOpen: false };

  handlerOpenMenu = () => {
    this.setState({ menuOpen: !this.state.menuOpen });
    document.body.style['overflow-y'] = !this.state.menuOpen ? 'hidden' : 'scroll';
  }

  navigateTo = (path) => {
    let _path = path;
    if (_path[0] === 'm') {
      this.handlerOpenMenu();
      _path = _path.substr(1);
    }

    // this.props.history.push(_path);
  }

  render () {
    return (
    <Wrapper>
      <LogoBlock to={'/'}>

        {!this.state.menuOpen && <StyledLogo />}
        {this.state.menuOpen && 'Меню'}

      </LogoBlock>
      <div><StyledMenu onClick={this.handlerOpenMenu} open={this.state.menuOpen} /></div>
      <NavBlock>
        <NavLink onClick={() => this.navigateTo('/')} selected={window.location.pathname === '/'} >Главная</NavLink>
        <NavLink onClick={() => this.navigateTo('/organizations')} selected={window.location.pathname.indexOf('organizations') !== -1} >Учреждения</NavLink>
        <NavLink onClick={() => this.navigateTo('/news')} selected={window.location.pathname.indexOf('news') !== -1}>Новости</NavLink>
        <NavLink onClick={() => this.navigateTo('/sections')} selected={window.location.pathname === '/sections'} >Секции</NavLink>
        {/* <NavLinkStub >Рейтинги</NavLinkStub> */}
        <PhoneBlock>
          <div>
            <img src={phone} alt="phone" />
          </div>
          <PhoneText href="tel:+74952307071">8 (495) 230-70-71</PhoneText>
        </PhoneBlock>
      </NavBlock>
      {this.state.menuOpen && (<MenuBlock>

          <NavLink onClick={() => this.navigateTo('m/')} selected={window.location.pathname === '/'} >Главная</NavLink>
          <NavLink onClick={() => this.navigateTo('m/organizations')} selected={window.location.pathname.indexOf('organizations') !== -1} >Учреждения</NavLink>
          <NavLink onClick={() => this.navigateTo('/news')} selected={window.location.pathname.indexOf('news') !== -1}>Новости</NavLink>
          <NavLink onClick={() => this.navigateTo('m/sections')} selected={window.location.pathname === '/sections'} >Секции</NavLink>
        <PhoneBlock>
          <div>
            <img src={phone} alt="phone" />
          </div>
          <PhoneText href="tel:+74952307071">8 (495) 230-70-71</PhoneText>
        </PhoneBlock>

      </MenuBlock>)}
    </Wrapper>
    );
  }
}
