import logo from './icons/logo.svg';
import logoGrey from './icons/logo_grey.svg';
import phone from './icons/phone.svg';
import phoneGrey from './icons/phone_grey.svg';
import bigLogo from './icons/bigLogo.svg';

import backGroundImg from './images/illu.jpg';
import noImage1 from './images/no-image1.png';
import noImage2 from './images/no-image2.png';
import noImage3 from './images/no-image3.png';
import noImage4 from './images/no-image4.png';
import noImage5 from './images/no-image5.png';
import zam1 from './images/zam1.png';
import zam2 from './images/zam2.png';
import director from './images/director.png';
import phototeams1 from './images/phototeams1.png';
import atlantLogo from './images/atlantLogo.png';
import ratmirLogo from './images/ratmir-logo.svg';

import { ArrowInSirlce,
    Arrow, ArrowDown, Edit, Save, Cancel, NoImagePlaceHolder, PlayBtn, ButovoLogo, PDFIcon, PhotoStub,
    WreathIcon, AvaFem, UDSImage, Logo, MenuIcon, FilterIcon, LikeIcon, Error, Error404 } from './svgIcons';
import facebook from './icons/social/facebook.svg';
import instagram from './icons/social/instagram.svg';
import ok from './icons/social/odnoklassniki.svg';
import twitter from './icons/social/twitter.svg';
import vk from './icons/social/vk.svg';
import g_facebook from './icons/social/gray/g-facebook.svg';
import g_instagram from './icons/social/gray/g-instagram.svg';
import g_ok from './icons/social/gray/g-odnoklassniki.svg';
import g_twitter from './icons/social/gray/g-twitter.svg';
import g_vk from './icons/social/gray/g-vk.svg';

const social = {
    icons: {
        facebook,
        instagram,
        ok,
        twitter,
        vk,
    },
    gray: {
        g_facebook,
        g_instagram,
        g_ok,
        g_twitter,
        g_vk,
    }
}

export { backGroundImg, Arrow, bigLogo, logo, logoGrey, phone, phoneGrey, ArrowInSirlce, NoImagePlaceHolder, PlayBtn,
    noImage1,
    noImage2,
    noImage3,
    noImage4,
    noImage5,
    WreathIcon,
    zam1,
    ArrowDown,
    zam2,
    director,
    ButovoLogo,
    PDFIcon,
    PhotoStub,
    phototeams1,
    AvaFem,
    UDSImage,
    atlantLogo,
    ratmirLogo,
    Logo,
    MenuIcon,
    social,
    Edit,
    Save,
    Cancel,
    FilterIcon,
    LikeIcon,
    Error,
    Error404,
};
