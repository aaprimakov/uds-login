import styled from 'styled-components';

import { TextInput } from '../components/textinput';

export const Wrapper = styled.div`
    width: 600px;
    padding-left: 40px;
    padding-top: 40px;
`;

export const StyledButton = styled.button`
    margin-top: 25px;
    border-radius: 28px;
    background-color: ${(props: { active: boolean }) => props.active ? '#003eff' : '#c2c2c2'};
    font-size: 18px;
    font-weight: bold;
    text-align: center;
    color: #ffffff;
    padding: 13px 34px;
    outline: none;
    cursor: pointer;
    border: none;
`;

export const StyledInput: any = styled(TextInput)`
  margin-top: 16px;
  color: #1e1d20;
  input {
    color: #1e1d20;
  }
`;