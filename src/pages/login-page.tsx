import * as React from 'react';
import axios from 'axios';

// export const history = createBrowserHistory();

import { StyledButton, StyledInput, Wrapper } from './styled';

export class AdminPage extends React.PureComponent<any, any> {
    constructor(props: any) {
        super(props);

        this.state = {
            username: '',
            password: '',
            submitted: false
        };
    }
    handleSubmit = (event: React.FormEvent) => {
        event.stopPropagation();
        event.preventDefault();

        //if (this.validate() && !this.validation()) {
        this.sendRequest();
        // }
    }

    sendRequest = () => {
        axios.post('users/authenticate', {
            username: this.state.username,
            password: this.state.password
        })
        .then((user) => {
            localStorage.setItem('user', JSON.stringify(user.data));
            this.props.openApp('uds-main/mainApp');
        })
        .catch();
    }

    setUsername = username => this.setState({ username });
    setPassword = password => this.setState({ password });

    render() {
        console.log('props', this.props);
        return (
            <Wrapper>
                <h1>Авторизация</h1>
                <form onSubmit={this.handleSubmit}>
                    <StyledInput
                        placeholder={'Имя пользователя'}
                        required={true}
                        onChange={this.setUsername}
                    />
                    <StyledInput
                        type="password"
                        placeholder={'Пароль'}
                        required={true}
                        onChange={this.setPassword}
                    />
                    <StyledButton active={true}>вход</StyledButton>
                </form>
            </Wrapper>
        );
    }
}