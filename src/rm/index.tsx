import * as React from "react";
import MetaTags from 'react-meta-tags';
import { connect } from 'react-redux';

import { login } from '../__data__/reducers';

import App from '..';

class ExampleApp extends React.PureComponent<any, any> {
    render() {
        // console.log('app.props', this.props);

        return (
            <React.Fragment>
                <MetaTags>
                    <title>Авторизация</title>
                    <meta name="description" content="Авторизуйтесь" />
                    <meta property="og:title" content="Вход" />
                </MetaTags>
                <App/>
            </React.Fragment>
        )
    }
}
const connectedApp = connect(null)(ExampleApp)
connectedApp['reducer'] = login;
export default connectedApp;